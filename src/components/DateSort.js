import React from 'react'
import PropTypes from 'prop-types'

const DateSort = props => {
  const { onClick, isSortingAsc } = props
  const activeArrowCls = 'stp-date-sort__arrow--active'

  return (
    <div onClick={onClick} className='stp-date-sort'>
      Sort by date
      <div className='stp-date-sort__arrows'>
        <i
          className={`stp-date-sort__arrow stp-date-sort__arrow--up ${
            isSortingAsc ? activeArrowCls : ''
          }`}
        />
        <i
          className={`stp-date-sort__arrow stp-date-sort__arrow--down ${
            !isSortingAsc ? activeArrowCls : ''
          }`}
        />
      </div>
    </div>
  )
}

DateSort.propTypes = {
  onClick: PropTypes.func,
  isSortingAsc: PropTypes.bool,
}

export default DateSort
