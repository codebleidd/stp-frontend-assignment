import { API_ENDPOINT } from 'config'

export function get (url, query) {
  // Get rid of unnecessary slashes if needed
  const apiBase = `${API_ENDPOINT.replace(/\/$/, '')}/`
  const resourceBase = `${url.replace(/^\//, '')}`

  return window.fetch(`${apiBase}${resourceBase}`)
}
